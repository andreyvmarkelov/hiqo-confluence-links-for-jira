/* 
 * Copyright (c) 2012, HiQo Solutions. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * <http://www.gnu.org/licenses/>
 * 
 * info@hiqo-solutions.com
 */
package com.hiqo.jira.confluence.plugin.beans;

/**
 * Class to store information about confluence link
 * @author alexander.kosko
 */
public class LinkInfo {

    private String name;
    private String url;
    private String space;
    private ConfluenceResource resource;

    /**
     * Public constructor
     * @param name
     * @param url 
     */
    public LinkInfo(String name, String url) {
        this.name = name;
        this.url = url;
    }

    /**
     * Medium constructor
     * @param name
     * @param url
     * @param space 
     */
    public LinkInfo(String name, String url, String space) {
        this.name = name;
        this.url = url;
        this.space = space;
    }

    /**
     * Full constructor
     * @param name
     * @param url
     * @param space
     * @param type 
     */
    public LinkInfo(String name, String url, String space, String type) {
        this.name = name;
        this.url = url;
        this.space = space;
        if ("attachment".equals(type)) {
            if (url != null) {
                String resourceType = url.substring(url.lastIndexOf(".") + 1, url.length());
                this.resource = ConfluenceResource.getConfluenceResource(type, resourceType);
            } else {
                this.resource = ConfluenceResource.ATTACHMENT_FILE;
            }
        } else {
            this.resource = ConfluenceResource.getConfluenceResource(type);
        }
    }

    /**
     * Returns name
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name
     * @param name 
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns URL
     * @return String
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets URL
     * @param name 
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Returns url of space.
     * @return String
     */
    public String getSpace() {
        return space;
    }

    /**
     * Sets url of space.
     * @param spaceUrl String
     */
    public void setSpace(String space) {
        this.space = space;
    }

    /**
     * Returns resource
     * @return ConfluenceResource
     */
    public ConfluenceResource getResource() {
        return resource;
    }
}