/* 
 * Copyright (c) 2012, HiQo Solutions. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * <http://www.gnu.org/licenses/>
 * 
 * info@hiqo-solutions.com
 */
package com.hiqo.jira.confluence.plugin.transport;

import com.atlassian.security.auth.trustedapps.EncryptedCertificate;
import com.atlassian.security.auth.trustedapps.TrustedApplicationUtils;
import com.atlassian.security.auth.trustedapps.TrustedApplicationsManager;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import org.apache.xmlrpc.DefaultXmlRpcTransport;

/**
 * XmlRpcTransport implementation for confluence.
 * @author alexander.kosko
 */
public class ConfluenceXmlRpcTransport extends DefaultXmlRpcTransport {

    private final String user;
    private final TrustedApplicationsManager trustedApplicationsManager;

    /**
     * Public constructor
     * @param url
     * @param user
     * @param trustedApplicationsManager 
     */
    public ConfluenceXmlRpcTransport(URL url, String user, TrustedApplicationsManager trustedApplicationsManager) {
        super(url);
        this.user = user;
        this.trustedApplicationsManager = trustedApplicationsManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputStream sendXmlRpc(byte[] request) throws IOException {
        con = url.openConnection();
        con.setRequestProperty("Content-Length", Integer.toString(request.length));
        con.setRequestProperty("Content-Type", "text/xml");
        if (auth != null) {
            con.setRequestProperty("Authorization", "Basic " + auth);
        }
        con.setUseCaches(false);
        con.setAllowUserInteraction(false);
        con.setDoInput(true);
        con.setDoOutput(true);
        if (user != null) {
            EncryptedCertificate certificate = trustedApplicationsManager.getCurrentApplication().encode(user);
            con.addRequestProperty(TrustedApplicationUtils.Header.Request.ID, certificate.getID());
            con.addRequestProperty(TrustedApplicationUtils.Header.Request.CERTIFICATE, certificate.getCertificate());
            con.addRequestProperty(TrustedApplicationUtils.Header.Request.SECRET_KEY, certificate.getSecretKey());
        }
        OutputStream out = con.getOutputStream();
        out.write(request);
        out.flush();
        out.close();
        return con.getInputStream();
    }
}