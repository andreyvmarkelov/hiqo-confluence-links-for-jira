/* 
 * Copyright (c) 2012, HiQo Solutions. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * <http://www.gnu.org/licenses/>
 * 
 * info@hiqo-solutions.com
 */
package com.hiqo.jira.confluence.plugin.beans;

import com.hiqo.jira.confluence.plugin.PluginConstants;
import java.text.ParseException;
import java.util.Comparator;
import java.util.Date;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;

/**
 * Class to compare 2 SearchResultItem objects by specified property
 * @author alexander.kosko
 */
public class SearchResultItemComparator implements Comparator<SearchResultItem>, PluginConstants {

    private Logger logger = Logger.getLogger(SearchResultItemComparator.class);
    private String comparableProperty;
    private boolean ascending;
    
    /**
     * Constructor with comparable property 
     * @param comparableProperty 
     */
    public SearchResultItemComparator(String comparableProperty) {
        this(comparableProperty, true);
    }

    /**
     * Constructor with comparable properties 
     * @param comparableProperty 
     */
    public SearchResultItemComparator(String comparableProperty, boolean ascending) {
        super();
        this.comparableProperty = comparableProperty;
        this.ascending = ascending;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public int compare(SearchResultItem item1, SearchResultItem item2) {
        int result;
        if (item1 == null) {
            result = 1;
        } else if (item2 == null) {
            result = -1;
        } else if (comparableProperty == null) {
            result = 0;
        } else {
            try {
                if ("lastModifiedDate".equals(comparableProperty)) {
                    Date property1;
                    Date property2;
                    try {
                        property1 = sdf.parse(BeanUtils.getProperty(item1, comparableProperty));
                    } catch (ParseException pe) {
                        return 1;
                    }    
                    try {
                        property2 = sdf.parse(BeanUtils.getProperty(item2, comparableProperty));
                    } catch (ParseException pe) {
                        return -1;
                    }    
                    result = property1.compareTo(property2);
                } else {    
                    result = BeanUtils.getProperty(item1, comparableProperty).compareTo(BeanUtils.getProperty(item2, comparableProperty));
                }    
            } catch (Exception ex) {
                logger.error("Error during comparison process.", ex);
                result = 0;
            }
        }
        if (!ascending) {
            result *= -1;
        }
        return result;
    }
}