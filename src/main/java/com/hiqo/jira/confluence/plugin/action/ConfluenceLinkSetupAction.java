/* 
 * Copyright (c) 2012, HiQo Solutions. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * <http://www.gnu.org/licenses/>
 * 
 * info@hiqo-solutions.com
 */
package com.hiqo.jira.confluence.plugin.action;

import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.hiqo.jira.confluence.plugin.manager.ConfluenceLinkConfigManager;

/**
 * Action to process confluence link configuration
 * @author alexander.kosko
 */
public class ConfluenceLinkSetupAction extends JiraWebActionSupport {

    private static final long serialVersionUID = -2830237754879183823L;
    private String processConfigUpdate;
    private String confluenceUrl;
    private int maxSearchResultsCount;
    private boolean updateSuccessful;

    /**
     * Public constructor
     */
    public ConfluenceLinkSetupAction() {
        super();
        this.confluenceUrl = ConfluenceLinkConfigManager.getInstance().getConfluenceUrl();
        this.maxSearchResultsCount = ConfluenceLinkConfigManager.getInstance().getMaxSearchResultsCount();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String doExecute() throws Exception {
        if (Boolean.valueOf(processConfigUpdate)) {
            ConfluenceLinkConfigManager.getInstance().setConfluenceUrl(getConfluenceUrl());
            ConfluenceLinkConfigManager.getInstance().setMaxSearchResultsCount(getMaxSearchResultsCount());
            updateSuccessful = true;
        }
        return super.doExecute();
    }

    /**
     * Sets processConfigUpdate flag
     * @param processConfigUpdate 
     */
    public void setProcessConfigUpdate(String processConfigUpdate) {
        this.processConfigUpdate = processConfigUpdate;
    }

    /**
     * Returns confluence URL
     * @return String
     */
    public String getConfluenceUrl() {
        return confluenceUrl;
    }

    /**
     * Sets confluence URL
     * @param confluenceUrl 
     */
    public void setConfluenceUrl(String confluenceUrl) {
        this.confluenceUrl = confluenceUrl;
    }

    /**
     * Indicates is configuration update successful.
     * @return boolean
     */
    public boolean isUpdateSuccessful() {
        return updateSuccessful;
    }

    /**
     * Returns maximum number of search records
     * @return integer
     */
    public int getMaxSearchResultsCount() {
        return maxSearchResultsCount;
    }

    /**
     * Sets maximum number of search records
     * @param maxSearchResultsCount 
     */
    public void setMaxSearchResultsCount(int maxSearchResultsCount) {
        this.maxSearchResultsCount = maxSearchResultsCount;
    }
}