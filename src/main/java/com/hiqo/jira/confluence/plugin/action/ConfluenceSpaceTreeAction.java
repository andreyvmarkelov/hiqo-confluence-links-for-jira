/* 
 * Copyright (c) 2012, HiQo Solutions. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * <http://www.gnu.org/licenses/>
 * 
 * info@hiqo-solutions.com
 */
package com.hiqo.jira.confluence.plugin.action;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.Permissions;
import com.atlassian.security.auth.trustedapps.TrustedApplicationsManager;
import com.hiqo.jira.confluence.plugin.beans.ConfluenceSpaceInfo;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import org.apache.log4j.Logger;

/**
 * Class to process actions for confluence space tree view
 *
 * @author alexander.kosko
 */
public class ConfluenceSpaceTreeAction extends AbstractConfluenceRefAction {

    private Logger logger = Logger.getLogger(ConfluenceSpaceTreeAction.class);
    private static final long serialVersionUID = -6690038231723416557L;
    public List<ConfluenceSpaceInfo> spaceList;
    private String linksFieldId;
    private String space;
    private String spaceName;
    boolean isFolder = false;
    String homePageId = "";
    String homePageTitle = "";
    String homePageUrl = "";
    String selectionValue = "";

    /**
     * Public constructor
     *
     * @param trustedApplicationsManager
     */
    public ConfluenceSpaceTreeAction(TrustedApplicationsManager trustedApplicationsManager) {
        super(trustedApplicationsManager);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String doExecute() throws Exception {
        String result = super.doExecute();
        if (getErrorMessages() == null || getErrorMessages().isEmpty()) {
            this.spaceList = getSpaceListFromConfluence("plugin.text.select.space");
            if (space == null) {
                Project currentProject = getUserProjectHistoryManager().getCurrentProject(Permissions.BROWSE, getLoggedInUser());
                String description = currentProject.getDescription();
                if (description != null) {
                    int beginIndex = description.indexOf("<cs>");
                    int endIndex = description.indexOf("</cs>");
                    try {
                        space = description.substring(beginIndex + 4, endIndex);
                    } catch (IndexOutOfBoundsException ex) {
                        space = "";
                    }
                }
            }
        }
        if (space != null && !space.trim().equals("")) {
            try {
                Vector params = new Vector<String>();
                params.add("");
                params.add(space);
                Object spaceObj = xmlRpcHandler.execute("confluence2.getSpace", params);
                if (spaceObj != null && spaceObj instanceof Map) {
                    Map<String, String> spaceData = (Map<String, String>) spaceObj;
                    homePageId = spaceData.get("homePage");
                    isFolder = hasChildren(homePageId);
                    spaceName = spaceData.get("name");
                    params = new Vector<String>();
                    params.add("");
                    params.add(homePageId);
                    Object pageObj = xmlRpcHandler.execute("confluence2.getPage", params);
                    if (pageObj != null && pageObj instanceof Map) {
                        Map<String, String> page = (Map) pageObj;
                        homePageTitle = page.get("title");
                        homePageUrl = page.get("url");
                        selectionValue = new StringBuffer("").append("name=").append(homePageTitle.replaceAll("\"", "&#34")).append(";url=").append(
                                page.get("url")).append(";type=page;space=").append(spaceName).append(";macroSearchId=").append(homePageId).append(";").toString();
                    }
                }
            } catch (Exception ex) {
                logger.error("Error while homepage is loaded: ", ex);
                addErrorMessage(ex.getMessage());
            }
        }
        return result;
    }

    /**
     * Returns the id of the custom field which contains confluence links
     *
     * @return String
     */
    public String getLinksFieldId() {
        return linksFieldId;
    }

    /**
     * Sets the id of the custom field which contains confluence links
     *
     * @param linksFieldId
     */
    public void setLinksFieldId(String linksFieldId) {
        this.linksFieldId = linksFieldId;
    }

    /**
     * Returns the current selected space id
     *
     * @return String
     */
    public String getSpace() {
        return space;
    }

    /**
     * Sets space id
     *
     * @param space
     */
    public void setSpace(String space) {
        this.space = space;
    }

    /**
     * Returns the list of spaces
     *
     * @return List<ConfluenceSpaceInfo>
     */
    public List<ConfluenceSpaceInfo> getSpaceList() {
        return spaceList;
    }

    /**
     * Returns home page id
     *
     * @return String
     */
    public String getHomePageId() {
        return homePageId;
    }

    /**
     * Returns home page title
     *
     * @return String
     */
    public String getHomePageTitle() {
        return homePageTitle;
    }

    /**
     * Returns value for home page selection
     *
     * @return String
     */
    public String getSelectionValue() {
        return selectionValue;
    }

    /**
     * Returns name of the selected space
     *
     * @return String
     */
    public String getSpaceName() {
        return spaceName;
    }

    /**
     * Returns the URL of home page
     *
     * @return String
     */
    public String getHomePageUrl() {
        return homePageUrl;
    }

    /**
     * Returns true if home page has children, false otherwise.
     *
     * @return
     */
    public boolean isIsFolder() {
        return isFolder;
    }
}