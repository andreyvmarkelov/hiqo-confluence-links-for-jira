/* 
 * Copyright (c) 2012, HiQo Solutions. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * <http://www.gnu.org/licenses/>
 * 
 * info@hiqo-solutions.com
 */
package com.hiqo.jira.confluence.plugin.action;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.security.auth.trustedapps.TrustedApplicationsManager;
import com.hiqo.jira.confluence.plugin.beans.ConfluenceSpaceInfo;
import com.hiqo.jira.confluence.plugin.manager.ConfluenceLinkConfigManager;
import com.hiqo.jira.confluence.plugin.transport.ConfluenceXmlRpcTransportFactory;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.XmlRpcHandler;

/**
 * Common class to process actions related to the Confluence
 *
 * @author alexander.kosko
 */
public class AbstractConfluenceRefAction extends JiraWebActionSupport {

    private static final long serialVersionUID = 5205841778407835444L;
    private Logger logger = Logger.getLogger(ConfluencePageSearchAction.class);
    protected XmlRpcHandler xmlRpcHandler;

    /**
     * Public constructor
     *
     * @param trustedApplicationsManager
     */
    public AbstractConfluenceRefAction(TrustedApplicationsManager trustedApplicationsManager) {
        try {
            URL xmlrpcUrl = new URL(ConfluenceLinkConfigManager.getInstance().getConfluenceUrl() + "rpc/xmlrpc");
            this.xmlRpcHandler = new XmlRpcClient(xmlrpcUrl, new ConfluenceXmlRpcTransportFactory(xmlrpcUrl, trustedApplicationsManager));
        } catch (MalformedURLException ex) {
            logger.error("Cannot parse xml rpc URL.", ex);
        }
    }

    /**
     * Returns a confluence URL
     *
     * @return String
     */
    public String getConfluenceUrl() {
        return ConfluenceLinkConfigManager.getInstance().getConfluenceUrl();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String doExecute() throws Exception {
        if ("".equals(ConfluenceLinkConfigManager.getInstance().getConfluenceUrl().trim())) {
            addErrorMessage(getText("plugin.error.confluence-link.undefined"));
        }
        if (ComponentManager.getInstance().getJiraAuthenticationContext().getLoggedInUser() == null) {
            addErrorMessage(getText("plugin.error.user-not-logged"));
        }
        return SUCCESS;
    }

    /**
     * Method to get the list of spaces from confluence
     */
    protected List<ConfluenceSpaceInfo> getSpaceListFromConfluence(String emptyOptionKey) {
        List<ConfluenceSpaceInfo> result = new ArrayList<ConfluenceSpaceInfo>();
        String emptyOption = "";
        if (emptyOptionKey != null && !emptyOptionKey.trim().equals("")) {
            emptyOption = getText(emptyOptionKey);
        }
        result.add(new ConfluenceSpaceInfo("", emptyOption));
        try {
            Vector params = new Vector<String>();
            params.add("");
            Object spaceListObj = xmlRpcHandler.execute("confluence1.getSpaces", params);
            if (spaceListObj instanceof List) {
                for (Map<String, String> space : (List<Map<String, String>>) spaceListObj) {
                    result.add(new ConfluenceSpaceInfo(space.get("key"), space.get("name")));
                }
            } else if (spaceListObj instanceof XmlRpcException) {
                addErrorMessage(((XmlRpcException) spaceListObj).getMessage());
                logger.error("An exception has been occurred till getting space list.", (XmlRpcException) spaceListObj);
            }
        } catch (Exception ex) {
            addErrorMessage(ex.getMessage());
        }
        return result;
    }

    /**
     * Method to check children for confluence page.
     *
     * @param pageId
     * @return boolean
     * @throws Exception
     */
    protected boolean hasChildren(String pageId) throws Exception {
        boolean result = false;
        Vector params = new Vector<String>();
        params.add("");
        params.add(pageId);
        Object nodeChildren = xmlRpcHandler.execute("confluence1.getChildren", params);
        result = nodeChildren instanceof List && (List) nodeChildren != null && ((List) nodeChildren).size() > 0;
        return result;
    }
}