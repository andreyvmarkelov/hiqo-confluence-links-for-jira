==============================
HiQo Confluence Links for JIRA
==============================

*******
Summary
*******
A custom JIRA Issue field with links to Confluence resources.

***********
Description
***********


This plugin allows to add links to Confluence resources into JIRA issues. The following resources can be linked:

* pages;
* attachments;
* blog posts;
* comments;
* spaces.

***************************
Additional Confluence Macro
***************************

There also is a matching Confluence user macro - HiQo JIRA Links Index. It can be inserted into a Confluence page to list all the JIRA issues that link to this page.

************
Wiki Content
************

* `Installation Guide`_
* `Plugin Configuration`_
* `Upgrading Plugin to Version 1.1`_
* `Using Plugin Source Code`_
* `User Guide`_
* `Confluence Macro Description`_

.. _Installation Guide:

Installation Guide
==================

*Note: To install HiQo Confluence Links for JIRA, the user must have admin rights.*

Prerequisites
_____________
1.You need to install JIRA (version 4.3 or higher) and Confluence.

2.Configure the connection between Confluence and JIRA as described below.

Configuring Confluence-JIRA Connection
______________________________________
1.Log in as a system administrator and go to the administration page. Select 'Application Links' in the administration menu. The 'Configure Application Links' page will appear, showing the application links that have been set up.

2.Click 'Add Application Link'.

.. figure:: https://bitbucket.org/HiQo/hiqo-confluence-links-for-jira/wiki/step1.jpg
   :align:  center

3.Enter the server URL of the application     you want to link to (the 'remote application').

4.Click the 'Next' button.

.. figure:: https://bitbucket.org/HiQo/hiqo-confluence-links-for-jira/wiki/step2.jpg
   :align: center
 
5.Enter the following information:

   - Enter the username and password of an administrator for the remote application.
   - 'Reciprocal Link URL' - The URL you specify here will override the base URL specified in your remote application's administration console, for the purposes of the application links connection. Application Links will use this URL to access the remote application.

6.Click the 'Next' button.

.. figure:: https://bitbucket.org/HiQo/hiqo-confluence-links-for-jira/wiki/step3.jpg
   :align: center

7.Enter the information required to configure authentication for your application link. 'The servers have the same set of users' or 'The servers have different sets of users' - Select one of these options depending on how you manage users between the two applications. 'These servers fully trust each other' - Tick this check box.

8.Click the 'Create' button to create the application link.

*Note: If you chose 'The servers have different sets of users', you should make following:*

    1. Check Anonymous Access to Remote API 

.. figure:: https://bitbucket.org/HiQo/hiqo-confluence-links-for-jira/wiki/security.jpg

   ..


    2.Enabling anonymous 'USE' permission 

.. figure:: https://bitbucket.org/HiQo/hiqo-confluence-links-for-jira/wiki/permissions.jpg
   :align: center


9.Connect Confluence to JIRA for User Management as described in http://confluence.atlassian.com/display/DOC/Connecting+to+Crowd+or+JIRA+for+User+Management#ConnectingtoCrowdorJIRAforUserManagement-jira.

10.Log in as an administrator to Confluenceand go to the administration page. Select 'General Configuration' in the administration menu.

11.Check Remote API (XML-RPC & SOAP).

.. figure:: https://bitbucket.org/HiQo/hiqo-confluence-links-for-jira/wiki/remoteAPI.jpg
   :align: center

Installation
____________
1.Log in as a system administrator and go to the Administration page. Select 'Plugins' in the administration menu.

2.The 'Administrator Access' login screen will be displayed. Enter your password and click Confirm.

3.Select the 'Install' tab.

.. figure:: https://bitbucket.org/HiQo/hiqo-confluence-links-for-jira/wiki/Administration-plugins.png
   :align: center

4.Click 'Upload Plugin'. The 'Upload Plugin' window will be displayed.

.. figure:: https://bitbucket.org/HiQo/hiqo-confluence-links-for-jira/wiki/SelectPlugin.png
   :align: center

*Note: Instead of downloading a JAR file, you can also click 'Atlassian Plugin Exchange' while on the 'Install' tab and search for the plugin there.*

5.Choose the JAR file you have downloaded. Click 'Upload'.

6.The 'Manage Existing' page will be displayed.

.. figure:: https://bitbucket.org/HiQo/hiqo-confluence-links-for-jira/wiki/InstalledPlugin.png
   :align: center

*Note: If you want install new version of the plugin, you must uninstall old version of the plugin and repeat steps 1-5. In case the field is not seen or data in the field are corrupted after the plugin upgrade, please try to disable and enable the plugin (this function is available on the Plugins->Manage Existing page).*

.. _Plugin Configuration:

Plugin Configuration
====================
1. Log in as a system administrator and go to the Administration page. Select 'Plugins'-> 'Other: Confluence Link Configuration' in the administration menu.
2. The 'Administrator Access' login screen will be displayed. Enter your password and click Confirm.
3. The 'Confluence Link Setup' page will be displayed. Fill in fields 'Confluence URL' and 'Max Search results count' and press 'Update Configuration'. If you will not fill 'Max search results count' it will be set to 50 by default.

.. figure:: https://bitbucket.org/HiQo/hiqo-confluence-links-for-jira/wiki/PluginConfiguration.png
   :align: center

To actually use the plugin, do the following:
_____________________________________________
1.Log in as a system administrator and go to the Administration page. Click 'Issues' in the administration menu.

2.The 'Administrator Access' login screen will be displayed. Enter your password and click Confirm.

3.Click 'Add Custom Field'. Choose 'Confluence Links Field' and click 'Next'.

.. figure:: https://bitbucket.org/HiQo/hiqo-confluence-links-for-jira/wiki/CreateFieldStep1.png
   :align: center

4.The 'Create Custom Field - Details' page will be displayed. Fill in the name (required, e.g. 'Confluence Links') and description (optional) for the field. Select types of issues it should be available for. Choose applicable context (e.g. certain projects, for which the field should be available). Click 'Finish'.

.. figure:: https://bitbucket.org/HiQo/hiqo-confluence-links-for-jira/wiki/CreateFieldStep2.PNG
   :align: center

5.The 'Screens' page will be displayed. Select 'Default Screen' and click 'Update'.

.. figure:: https://bitbucket.org/HiQo/hiqo-confluence-links-for-jira/wiki/CreateFieldStep3.png
   :align: center

.. _Upgrading Plugin to Version 1.1:

Upgrade 'HiQo Confluence Links for JIRA' plugin to version 1.1
==============================================================
1. Log in as a system administrator and go to the administration page. Click 'Plugins' in the administration menu.
2. The "Administrator Access" login screen will be displayed. Enter your password and click 'Confirm'.
3. Click 'Upgrade'.
4. The 'Available Upgrades' page will be displayed. Select 'HiQo Confluence Links for JIRA' plugin and click 'Upgrade Now'. 

*After that you'll need to edit your Custom Field. For that, do the following:*

5. Click 'Issues -> Fields' in the administration menu.
6. The 'Administrator Access' login screen will be displayed. Enter your password and click 'Confirm'.
7. The 'View Custom Fields' page will be displayed. Click 'Edit' next to your Custom Field.
8. The 'Edit Custom Field Details' page will be displayed. In the field Search Template select 'Search Confluence links'. Then click 're-index' and 'Update'.
 
*After that you'll be able to search for issues by your Custom field (the way this can be done is described in User Guide).*

.. _Using Plugin Source Code:

Using Plugin Source Code
========================
In case you want to use our plugin source code, you should replace your properties (placed in pom.xml) with properties below:

**<properties>**

        **<JIRA.version>4.4.3</JIRA.version>**

        **<JIRA.data.version>4.4</JIRA.data.version>**

        **<JIRA.plugin.sdk.url>$atlassian_sdk_repository$</JIRA.plugin.sdk.url>**

        **<amps.version>3.7.2</amps.version>**

        **<maven.local.repo>$maven_repository_path$ </maven.local.repo>**

        **<confluence.version>4.0</confluence.version>**

        **<confluence.data.version>4.0</confluence.data.version>**

**</properties>**

$atlassian_sdk_repository$ and $maven_repository_path$  represent a path to the Atlassian SDK repository (for example *'file:///D:/Work/atlassian-plugin-sdk-3.7.2/repository/'* and  *'D:\Work\atlassian-plugin-sdk-3.7.2\repository'*, respectively).

.. _User Guide:

User Guide
==========

This page contains basic instructions for using the plugin in a certain JIRA issue.

Add Links to JIRA Issues
________________________

1.Open a JIRA issue in edit mode.

2.Click the 

.. figure:: https://bitbucket.org/HiQo/hiqo-confluence-links-for-jira/wiki/plus.png
   :align: center 

icon near the Confluence Links field. A search pop-up will appear.

.. figure:: https://bitbucket.org/HiQo/hiqo-confluence-links-for-jira/wiki/EditIssue.png
   :align: center

3.Type the search term into the search field in the pop-up. Choose a particular Confluence Space if needed.

*Note: The search term cannot be blank. For better results, specify at least 3 letters.*

.. figure:: https://bitbucket.org/HiQo/hiqo-confluence-links-for-jira/wiki/SearchStart.png
   :align: center

   Press the 'Search' button. All relevant results will be displayed in the table below.

*Note:you can sort search results by clicking on the column titles.*

.. figure:: https://bitbucket.org/HiQo/hiqo-confluence-links-for-jira/wiki/SearchResultsPopup.png
   :align: center


4.Check necessary items and press the 

5.Parameters describing the selected links will be added to the Confluence Links field.

*Note: if you already had some links added to the issue, the links you've just selected will be added to the field.*

.. figure:: https://bitbucket.org/HiQo/hiqo-confluence-links-for-jira/wiki/EditFieldWithData.png
   :align: center

6.Press 'Update'. The links will be displayed in the 'Details' sections of the issue.

.. figure:: https://bitbucket.org/HiQo/hiqo-confluence-links-for-jira/wiki/ViewIssue.png
   :align: center

7.From version 1.3, 1 more way is exist to add confluence resources to Jira issue, called 'Tree view'. It is opened by the default and the user can select the confluence's pages by using the navigation tree at the selected confluence space.

.. figure:: https://bitbucket.org/HiQo/hiqo-confluence-links-for-jira/wiki/TreeView.png 
   :align: center

If desired, the user can switch to 'Search View'. 

.. figure:: https://bitbucket.org/HiQo/hiqo-confluence-links-for-jira/wiki/SearchView.png
   :align: center

*Note: To make some confluence space selected by the default when 'Tree View' is opened, the user should add to project description the key of corresponding confluence space placed in the tag <cs>...</cs>*
 
Edit/Delete links from a JIRA issue
___________________________________

1. Open the JIRA issue in edit mode.
2. To delete a link, remove the corresponding document line from the Confluence Links field. To edit the displayed name of the link, change its 'name' parameter.
3. Press the Update button, the changes will be applied to the issue.

.. _Confluence Macro Description:

HiQo JIRA Links Index Macro
===========================

Prerequisites: this Confluence user macro will work correctly only if 'HiQo Confluence Links' plugin is installed on your JIRA.

Installation Guide
__________________

1. Log in to Confluence as a system administrator and go to the administration page. Click 'User Macros' in the administration menu. The 'User Macros' page will appear, showing the user macros that have been previously installed.
2. Click 'Create a User Macro'. 
3. Enter the name (e.g. hiqolinks) and title (e.g. HiQo Confluence Links) of your macro and select a category where this macro should appear.
4. Select 'Rendered' for the Macro Body Processing. Copy the following code in the Template field:

**## Macro title: Confluence Links search in Jira**

**## Macro has a body: Y or N**

**## Body processing: Selected body processing option**

**## Output: Selected output option**

**##**

**## Developed by: HiQo Solutions**

**## Date created: 02/08/2012**

**## @noparams**

**#set($searchParam='"macroSearchId=' +$content.id + '"')**

**<ac:macro ac:name="jira">**

    **<ac:parameter ac:name="title">JIRA issues linked to $content.getTitle()</ac:parameter>**

    **<ac:parameter ac:name="url">http://[your-jira-server-and-port]/sr/jira.issueviews:searchrequest-xml/temp/SearchRequest.xml?jqlQuery="[your-custom-field-name]"+%7E+$searchParam+ORDER+BY+resolutiondate&tempMax=1000</ac:parameter>**

    **<ac:parameter ac:name="columns">type;key;summary;priority;status;resolution;created;resolved;fixversion</ac:parameter>**

    **<ac:parameter ac:name="cache">off</ac:parameter>**

**</ac:macro>**

5. Change the following:

   * **[your-jira-server-and-port]** to your server name and port (if necessary);
   * **[your-custom-field-name]** to the name of your Custom Field that has been created in JIRA.\\

6. Click 'Save'.

User Guide
__________

After you've installed the user macro, you will be able to add it to any wiki page the same way it's done for standard macros (either select Insert - Other Macro and find the macro in the dialog or start typing '{' and the macro name to see autocomplete in the Confluence editor).
The output of the macro on the wiki page will look as follows:

.. figure:: https://bitbucket.org/HiQo/hiqo-confluence-links-for-jira/wiki/Macro.jpg
   :align: center